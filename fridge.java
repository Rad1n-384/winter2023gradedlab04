public class fridge
{
 private String color;
 private int height;
 private int coolingpower;
 
 public fridge (String color, int height, int coolingpower)
 {
	 this.color = color;
	 this.height = height;
	 this.coolingpower = coolingpower;
 }
 public int keepCold()
 {
	 System.out.println("I keep stuff cool with my "+ coolingpower+"cooling power");
	 return coolingpower;
 }
 
 public String makeYouLookRich()
 {
	 System.out.println("I make you look rich because of my "+color+"color");
	 return color;
 }
 
 private boolean validate(int power)
 {
  boolean canuse = false;
   if(power > 0 && power < coolingpower)
   {
	 canuse = true;
   }
   return canuse;
 }
 
 public int savePower(int power)
 {
  if(validate(power))
  {
	this.coolingpower = coolingpower - power;
  }
  return coolingpower;
 }
 
 public String getColor()
  {
	return this.color;
  }
  
  public int getHeight()
  {
	 return this.height;
  }
  
  public int getCoolingpower()
  {
	 return this.coolingpower;
  }
  
  public void setColor(String color)
  {
	 this.color = color;
  }
  
  public void setHeight(int height)
  {
	 this.height = height;
  }
  
  public void setCoolingpower(int coolingpower)
  {
	 this.coolingpower = coolingpower;
  }
}